-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2018 a las 15:27:49
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` varchar(200) NOT NULL,
  `publication_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comment`
--

INSERT INTO `comment` (`id`, `comment`, `name`, `date`, `publication_id`) VALUES
(1, 'No me gustó la publicación', 'Bryan Taboada', '2/10/2018', 3),
(2, 'Me encanta este tipo de contenido  sigan asi', 'Elizabeth Agila', '2/10/2018', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` varchar(600) NOT NULL,
  `status` int(2) NOT NULL,
  `image_url` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publication`
--

INSERT INTO `publication` (`id`, `title`, `date`, `description`, `content`, `status`, `image_url`, `user_id`) VALUES
(2, 'Creación de servidor apache con Docker', '01/10/2018', 'Creación de servidor apache con Docker, en un hosting contratado con un dominio especifico 100% real no fake un enlace en mega ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tristique, tortor vel pellentesque posuere, est elit egestas mi, in pellentesque tellus nibh vehicula arcu. Sed viverra nibh nec commodo imperdiet. Vivamus pretium dui ut lorem semper, vel interdum justo tincidunt. Maecenas diam nibh, tincidunt sit amet pellentesque ac, pulvinar at elit. Donec risus purus, lobortis commodo metus ac, sodales tincidunt nisi. Aliquam consectetur, nibh convallis fringilla dictum, ante neque tempor diam, a elementum orci turpis in ex. Vivamus augue lacus, finibus ac gravida eu, venenatis nec tortor. P', 1, 'https://cdn-images-1.medium.com/max/2000/1*JAJ910fg52ODIRZjHXASBQ.png', 1),
(3, 'Examen de diagnostico', '24/09/2018', 'Primer test de diagnostico de html5, javascript, y css3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum a porttitor est. Aenean eu congue mi. Phasellus hendrerit molestie faucibus. Aenean euismod mi vel augue laoreet, ut consectetur enim pellentesque. Cras aliquam diam fermentum aliquam bibendum. Nullam ut ante metus. Mauris semper malesuada risus. Sed at augue neque.', 1, 'https://media.istockphoto.com/vectors/illustration-of-a-mad-scientist-making-a-bubbly-green-potion-vector-id163920418?k=6&m=163920418&s=612x612&w=0&h=o-E1GvHDCm8BKJexSl0znvrWw094USimLs5UE2g7Aac=', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `role` int(2) NOT NULL,
  `description` varchar(500) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `auth_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `nombre`, `username`, `password_hash`, `role`, `description`, `image`, `status`, `auth_key`) VALUES
(1, 'Oscar Vinueza', 'os.vinueza94@gmail.com', '$2y$12$OhX0eiZpi65Iieoq.pEofuMyJbl86uW6sxT/lwZ4CcdRRMFJlkKEu', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue sed purus id tempus. Vestibulum scelerisque placerat auctor. Suspendisse ornare ut est sodales cursus. Nullam vel imperdiet orci. In nec mauris eu eros posuere viverra id quis est. Nam lacinia at diam ac bibendum. Fusce ut leo ut felis auctor tincidunt ut aliquet mi. Nulla ut sapien id lectus dictum suscipit vel vel nibh. Vivamus ut lorem id dolor aliquet euismod. Ut lorem odio, mattis eget dolor et, tincidunt mattis ligula.', 'https://scontent.fuio1-1.fna.fbcdn.net/v/t1.0-9/16708182_1445627885470903_7215861514568363341_n.jpg?_nc_cat=111&oh=9668985ca30f055c2860ecaf3b96834c&oe=5C587386', 10, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
