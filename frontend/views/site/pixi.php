<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<script src="<?= \yii\helpers\Url::to('@web/js/pixi.min.js')?>"></script>
<div class="page-container contact-section float-right" id="contenedor"></div>
<script type="text/javascript">
    
   // The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container

const app = new PIXI.Application(625,500);
//Agregamos un fondo a la escena
var background = PIXI.Sprite.fromImage('<?= yii\helpers\Url::to('@web/images/fondoJuego.jpg')?>');
background.width = app.screen.width;
background.height = app.screen.height;
// add background to stage...
app.stage.addChild(background);

// load the texture we need
PIXI.loader.add('bunny', '<?= yii\helpers\Url::to('@web/images/cat.png')?>').load((loader, resources) => {
    // This creates a texture from a 'bunny.png' image
    const bunny = new PIXI.Sprite(resources.bunny.texture);

    // Setup the position of the bunny
    bunny.x = 0;
    bunny.y = 0;

    // Rotate around the center
    bunny.anchor.x = 0.5;
    bunny.anchor.y = 0.5;

    // Add the bunny to the scene we are building
    app.stage.addChild(bunny);

    // Listen for frame updates
    app.ticker.add(() => {
         // each frame we spin the bunny around a bit
        bunny.rotation += 0.01;
        bunny.x +=1;
        bunny.y +=1;
    });
    
});

//Agregamos texto animado
var style = new PIXI.TextStyle({
    fontFamily: 'Arial',
    fontSize: 36,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradient
    stroke: '#4a1850',
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
    wordWrap: true,
    wordWrapWidth: 440
});

var richText = new PIXI.Text('Ejemplo con Pixi', style);
richText.x = 170;
richText.y = 0;

app.stage.addChild(richText);

//agregando a Jack
var texture = PIXI.Texture.fromImage('<?= yii\helpers\Url::to('@web/images/jack/Idle (1).png')?>');

// Scale mode for pixelation
texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;

// create our little bunny friend..
var bunny = new PIXI.Sprite(texture);

// enable the bunny to be interactive... this will allow it to respond to mouse and touch events
bunny.interactive = true;

// this button mode will mean the hand cursor appears when you roll over the bunny with your mouse
bunny.buttonMode = true;

// center the bunny's anchor point
bunny.anchor.set(0.5);

// make it a bit bigger, so it's easier to grab
bunny.scale.set(0.5);

// setup events for mouse + touch using
// the pointer events
bunny
    .on('pointerdown', onDragStart)
    .on('pointerup', onDragEnd)
    .on('pointerupoutside', onDragEnd)
    .on('pointermove', onDragMove);

bunny.x = 300;
bunny.y = 300;

// add it to the stage
app.stage.addChild(bunny);

function onDragStart(event) {
    // store a reference to the data
    // the reason for this is because of multitouch
    // we want to track the movement of this particular touch
    this.isdown = true;
    this.texture = PIXI.Texture.fromImage('<?= yii\helpers\Url::to('@web/images/jack/Run (7).png')?>');
    this.data = event.data;
    this.alpha = 0.5;
    this.dragging = true;
}

function onDragEnd() {
    this.isdown = false;
    this.texture = PIXI.Texture.fromImage('<?= yii\helpers\Url::to('@web/images/jack/Idle (1).png')?>');
    this.alpha = 1;
    this.dragging = false;
    // set the interaction data to null
    this.data = null;
}

function onDragMove() {
    if (this.dragging) {
        var newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x;
        this.y = newPosition.y;
    }
}   
document.body.childNodes[9].childNodes[3].childNodes[5].appendChild(app.view);
</script>