<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
?>
<div class="page-container contact-section float-right">
      		<div class="row d-flex mb-5 contact-info">
            <?php if(!empty(Yii::$app->session->getFlash('error'))){ echo '<div class="alert alert-danger" role="alert"><strong>'.Yii::$app->session->getFlash('error').'</strong></div>';} ?>
            <div class="col-md-12 mb-4">
                <h2 class="h1" style="color:chocolate">Información de contácto</h2>
            </div>
            <div class="w-100"></div>
            <div class="col-md-6">
              <p><span style="color:chocolate">Dirección:</span> Calle espejo, subida a santa cruz</p>
            </div>
            <div class="col-md-6">
              <p><span style="color:chocolate">Teléfono:</span>0982657354</p>
            </div>
            <div class="col-md-6">
              <p><span style="color:chocolate">Email:</span> os.vinueza94@gmail.com</p>
            </div>            
          </div>
          <div class="row block-9">
            <div class="col-md-12 mb-5">
                
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'provincia_id')->dropDownList(ArrayHelper::map(\common\models\Provincia::find()->all(), 'idProvincia', 'nombreProvincia'), [
                    'prompt'=> Yii::t('app','Seleccione provincia'),
                    'class'=>'form-control',
                    'onchange'=> '
                    //jsShowWindowLoad("Seleccionando....<br>");
                    $.post( "'.Yii::$app->urlManager->createUrl('site/provincias?id=').'"+$(this).val(), function( data ) {
                      $( "select#contact-canton_id" ).html( data );
                      //jsRemoveWindowLoad();
                    });',
                ])->label('Provincia') ?>
                
                <?= $form->field($model, 'canton_id')->dropDownList($model->canton_id ? ArrayHelper::map(\common\models\Canton::find()->all(), 'idCanton', 'nombreCanton') : ['' => Yii::t('app', 'Seleccione cantón')])->label('Cantón')?>        

                <?= $form->field($model, 'message')->widget(CKEditor::className(),[
                    'options' => [
                        'preset' => 'full',
                        'inline' => false,
                    ],
                ]) ?>
                
                <div class="g-recaptcha" data-sitekey="6LeO93YUAAAAAA-NL0uF-4qRblRtuxBudaMSlpuG"></div>
                
                <div class="form-group">
                    <?= Html::submitButton('Enviar mensaje', ['class' => 'btn btn-primary']) ?>
                </div>
                
                <?php ActiveForm::end(); ?>
            
            </div> 
              <div class="col-md-12 mb-5">
                  <iframe class="float-right" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1994.622681497587!2d-79.65749945394137!3d0.9710182988215681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8fd4bc04094fc5d1%3A0x81e8ec9e95518596!2sPontificia+Universidad+Cat%C3%B3lica+del+Ecuador+-+Sede+Esmeraldas!5e0!3m2!1ses-419!2sec!4v1539104346017" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen="true"></iframe>                  
              </div>
          </div>
      	</div><!-- end: page-container-->          	
        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen">
              <svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg>
        </div>
        <!--<div style="width: 50%;height:450px"></div>-->        
  	