<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<script src="<?=\yii\helpers\Url::to('@web/js/crafty-min.js')?>"></script>
<div id="game"></div>
<script>
    Crafty.init(300,300, document.getElementById('game'));
    Crafty.background("#3396FF");

Crafty.e('Floor, 2D, Canvas, Color')
  .attr({x: 0, y: 250, w: 250, h: 10})
  .color('green');

var square=Crafty.e('2D, Canvas, Color, Twoway, Gravity,Mouse')
  .attr({x: 0, y: 0, w: 50, h: 50})
  .color('red')
  .twoway(200)
  .gravity('Floor');
  
square.bind("Click", function() {
    this.color("blue");
  });

</script>

  