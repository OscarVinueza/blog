<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="page-container float-right">
      		<div class="row about-section">
      			<div class="col-md-6 ftco-animate">
                            <img style="margin-top: 80px" src="https://d500.epimg.net/cincodias/imagenes/2017/07/07/midinero/1499441084_581226_1499441878_miniatura_normal.jpg" class="img-fluid" alt="">
      			</div>
      			<div class="col-md-6 ftco-animate">
      				<h2 class="mb-4">Anubis</h2>
      				<p>Anubis es un blog estudiantil, en el cual se publica información interesante a cerca de temas tecnológicos en los que se ven envueltos estudiantes de 6to nivel de la carrera de Ingenierda en Sistemas y Computación de la Pontificia Universidad Católica del Ecuador con sede en Esmeraldas. </p>
      			</div>
      			<div class="col-md-12 mt-5 ftco-animate">
      				<p>Nuestro objetivo es dar a conocer estos temas mostrando nuestras experiencias y describiendo como es el trabajo con cada una de las herramientas manejadas y los nuevos temas que a diario conocemos.</p>
      				<h3 class="mb-4 mt-5">Síguenos aquí:</h3>
      				<ul class="ftco-footer-social list-unstyled">
                <li class="ftco-animate"><a href="https://twitter.com/CodecWorld" target="_blank"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/Jostechq-211434169558564/" target="_blank"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/vinuezaceli/?hl=es-la" target="_blank"><span class="icon-instagram"></span></a></li>
              </ul>
      			</div>
      		</div>
      	</div><!-- end: page-container-->
      </section>
    	
		  <!-- loader -->
		  <div id="ftco-loader" class="show fullscreen">
		  	<svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg>
                  </div>  	