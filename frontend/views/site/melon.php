<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<script src="<?= \yii\helpers\Url::to('@web/js/melonjs.min.js')?>"></script>
<div class="page-container contact-section float-right" id="contenedor"></div>
<script>
    var game = {
    // game assets
    assets : [
        { name: "bob",   type:"image", src:"https://66.media.tumblr.com/45d880b15985fe10d9bd6f129cc8f700/tumblr_o8dqugZsu31uwt91no1_640.png" },        
        { name: "langosta",   type:"image", src:"https://vignette.wikia.nocookie.net/mugen-base-de-datos/images/c/ca/200px-Patrick_Star_svg.png/revision/latest?cb=20150425202543&path-prefix=es" },        
    ],

    onload: function()
    {
        // Initialize the video.
        if (!me.video.init(1024, 768, {wrapper : "screen", scale : "auto"})) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }

        // set all resources to be loaded
        me.loader.preload(game.assets, this.loaded.bind(this));
    },

    loaded: function () {
        // set the "Play/Ingame" Screen Object
        me.state.set(me.state.PLAY, new PlayScreen());

        // switch to PLAY state
        me.state.change(me.state.PLAY);
    }
};

var PlayScreen = me.ScreenObject.extend( {
    onResetEvent: function() {
         // clear the background
        me.game.world.addChild(new me.ColorLayer("background", "#5E3F66", 0), 0);

        me.game.world.addChild(new Smilie(0), 3);
        me.game.world.addChild(new Smilie(1), 3);
    }
});

var Smilie = me.Entity.extend({
    init : function (i) {
        this._super(
            me.Entity,
            "init",
            [
                me.Math.random(-15, 1024),
                me.Math.random(-15, 768),
                {
                    width : 12,
                    height : 12,
                    shapes : [ new me.Ellipse(2, 2, 4, 4) ]
                }
            ]
        );

        // disable gravity and add a random velocity
        this.body.gravity = 0;
        this.body.vel.set(me.Math.randomFloat(-4, 4), me.Math.randomFloat(-4, 4));

        this.alwaysUpdate = true;

        // add the coin sprite as renderable
        this.renderable = new me.Sprite(0, 0, {image: me.loader.getImage(game.assets[i % 5].name)});
    },

    update : function () {
        this.pos.add(this.body.vel);

        // world limit check
        if (this.pos.x >= 1024) {
            this.pos.x = -15;
        }
        if (this.pos.x < -15) {
            this.pos.x = 1024 - 1;
        }
        if (this.pos.y >= 768) {
            this.pos.y = -15;
        }
        if (this.pos.y < -15) {
            this.pos.y = 768 - 1;
        }

        if (me.collision.check(this)) {
            // me.collision.check returns true in case of collision
            this.renderable.setOpacity(1.0);
        }
        else {
            this.renderable.setOpacity(1.0);
        }
        return true;
    },

    // collision handler
    onCollision : function (response) {

        this.pos.sub(response.overlapN);
        if (response.overlapN.x !== 0) {
            this.body.vel.x = me.Math.randomFloat(-4, 4) * -Math.sign(this.body.vel.x);
        }
        if (response.overlapN.y !== 0) {
            this.body.vel.y = me.Math.randomFloat(-4, 4) * -Math.sign(this.body.vel.y);
        }

        return false;
    }
});

/* Bootstrap */
me.device.onReady(function onReady() {
    game.onload();
    document.body.childNodes[9].childNodes[3].childNodes[5].appendChild(document.body.childNodes[10]);
});   
</script>