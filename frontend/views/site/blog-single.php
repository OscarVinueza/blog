<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
      	<div class="page-container float-right" id="contenedor">
      		<div class="row">
            <div class="col-md-12">
              <?php
              echo '<h2 class="mb-3">'.$publicacion->title.'</h2>
              <p>'.$publicacion->description.'</p>
              <p>
                <img src="'.$publicacion->image_url.'" alt="" class="img-fluid">
              </p>
              <p>'.$publicacion->content.'</p>';
              ?>                            
              
              <div class="about-author d-flex pt-5">
                <div class="bio align-self-md-center mr-4">
                    <img src="<?= app\models\User::findOne($publicacion->user_id)->image?>" alt="Image placeholder" class="img-fluid mb-4">
                </div>
                <div class="desc align-self-md-center">
                  <h3>Acerca del autor</h3>
                  <p><?= app\models\User::findOne($publicacion->user_id)->description?></p>
                </div>
              </div>


              <div class="pt-5 mt-5">
                <h3 class="mb-5"><?=count($comentarios)?> comentarios</h3>
                <ul class="comment-list">
                    <?php
                    foreach($comentarios as $comentario){
                        echo '<li class="comment">                    
                        <div class="comment-body">
                          <h3>'.$comentario->name.'</h3>
                          <div class="meta">'.$comentario->date.'</div>
                          <p>'.$comentario->comment.'</p>                          
                        </div>
                      </li>';
                    }
                    ?>                                    
                </ul>
                <!-- END comment-list -->
                
                <div class="comment-form-wrap pt-5">
                  <h3 class="mb-5">Deja un comentario</h3>
                  <?php
                    use yii\helpers\Html;
                    use yii\widgets\ActiveForm;

                    $form = ActiveForm::begin([
                        'id' => 'login-form',
                        //'options' => ['class' => 'form-horizontal'],
                    ]) ?>
                        <?= $form->field($model, 'comment') ?>
                        <?= $form->field($model, 'name') ?>                                               
                  <br>
                        <div class="form-group">                            
                                <?= Html::submitButton('Enviar comentario', ['class' => 'btn py-3 px-4 btn-primary','style'=>'background:chocolate;color:white']) ?>                            
                        </div>                  
                    <?php ActiveForm::end() ?>
                </div>
              </div>
            </div> <!-- .col-md-12 -->
          </div>
      	</div><!-- end: page-container-->
      </section>
    	
		  <!-- loader -->
		  <div id="ftco-loader" class="show fullscreen">
		  	<svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg>
		  </div>

  	</div>