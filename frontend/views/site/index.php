<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use common\models\User;
use yii\widgets\LinkPager;
?>
      	<div class="page-container float-right">
      		<div class="row">
                        <div class="col-md-6">
                            <?php                            
                            foreach($pubLeft as $item){                                
                                $username=User::findOne($item->user_id)->nombre;
                                echo '<div class="blog-entry ftco-animate">
                                <a href="site/blog/'.$item->id.'" class="blog-image">
                                        <img src="'.$item->image_url.'" class="img-fluid" alt="">
                                </a>
                                <div class="text py-4">
                                  <div class="meta">
                                    <div>'.$item->date.'</div>
                                    <div>'.$username.'</div>
                                  </div>
                                  <h3 class="heading"><a style="color:chocolate" href="site/blog/'.$item->id.'">'.$item->title.'</a></h3>
                                  <p>'.$item->description.'</p>
                                </div>
                              </div>';
                            }
                            ?>             
      			</div>
                        <div class="col-md-6">
                            <?php                            
                            foreach($pubRight as $item){                                
                                $username=User::findOne($item->user_id)->nombre;
                                echo '<div class="blog-entry ftco-animate">
                                <a href="site/blog/'.$item->id.'" class="blog-image">
                                        <img src="'.$item->image_url.'" class="img-fluid" alt="">
                                </a>
                                <div class="text py-4">
                                  <div class="meta">
                                    <div>'.$item->date.'</div>
                                    <div>'.$username.'</div>
                                  </div>
                                  <h3 class="heading"><a style="color:chocolate" href="site/blog/'.$item->id.'">'.$item->title.'</a></h3>
                                  <p>'.$item->description.'</p>
                                </div>
                              </div>';
                            }
                            ?>             
      			</div>
      		</div>
            <?php
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>
        </div><!-- end: page-container-->
      </section>
    	
		  <!-- loader -->
		  <div id="ftco-loader" class="show fullscreen">
		  	<svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg>
		  </div>

  	</div>