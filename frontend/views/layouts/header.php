<?php
use yii\helpers\Html;
?>
<nav id="colorlib-main-nav" role="navigation" style="background: chocolate">
      <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
      <div class="js-fullheight colorlib-table">
        <div class="colorlib-table-cell js-fullheight">
          <div class="row d-flex justify-content-end">
            <div class="col-md-12 px-5">
              <ul class="mb-5">
                <li class="active">
                    <?= Html::a('<span>Inicio</span>', ['/']) ?>
                </li>                
                <li><a href="<?= yii\helpers\Url::to('@web/site/about')?>"><span>Acerca de nosotros</span></a></li>
                <li><a href="<?= yii\helpers\Url::to('@web/site/contact')?>"><span>Contácto</span></a></li>
              </ul>
            </div>            
          </div>
        </div>
      </div>
    </nav>
    <div id="colorlib-page">
      <header>
      	<div class="container-fluid">
	        <div class="row no-gutters">
	          <div class="col-md-12">
	            <div class="colorlib-navbar-brand">
                      <?= Html::a('Anubis',['/'],['class'=>'colorlib-logo'])?>
	            </div>
	            <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
	          </div>
	        </div>
        </div>
      </header>

      <section class="ftco-fixed clearfix">
      	<div class="image js-fullheight float-left">
      		<div class="home-slider owl-carousel js-fullheight">
		        <div class="slider-item js-fullheight" style="background-image: url('https://w-dog.net/wallpapers/13/5/336773600042188.jpg');">
		          <div class="overlay"></div>
		          <div class="container">
		            <div class="row slider-text align-items-end" data-scrollax-parent="true">
		              <div class="col-md-10 col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
		              	<p class="cat"><span></span></p>
		                <h1 class="mb-3" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"></h1>
		              </div>
		            </div>
		          </div>
		        </div>

		        <div class="slider-item js-fullheight" style="background-image: url('https://images3.alphacoders.com/813/813087.jpg');">
		          <div class="overlay"></div>
		          <div class="container">
		            <div class="row slider-text align-items-end" data-scrollax-parent="true">
		              <div class="col-md-10 col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">		              			                
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
      	</div>
