/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';   

class Publication extends React.Component {
    
    constructor(props){
        super(props);        
    }
           
    componentDidMount(){  
        var x=this.props.state.publications[this.props.state.page].content;
        var extractscript=/<script>(.+)<\/script>/gi.exec(x);
        if(extractscript!=null){
            const s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.innerHTML = extractscript[1];
            document.body.appendChild(s);
        }        
    }
    
    handleComment(){        
        this.props.sendComment(this.refs.commentComment.value);
    } 
    
    render(){  
        let comments;
        comments=this.props.state.comments.map((comment,i)=>{
                return(     
                    <li className="comment" key={i}>
                     <div className="vcard bio">                      
                     </div>
                     <div className="comment-body">
                         <h3>{comment.name}</h3>
                         <div className="meta">{comment.date}</div>
                         <p>{comment.comment}</p>
                     </div>                    
                   </li>                                      
                 );
             });
        var x=this.props.state.publications[this.props.state.page].content;
        var extractscript=/<script>(.+)<\/script>/gi.exec(x);
        if(extractscript!=null){
            x=x.replace(extractscript[0],"");
        }        
    return(
        <div className="col-md-12">
            <h2 className="mb-3">{this.props.state.publications[this.props.state.page].title}</h2>
            <p>{this.props.state.publications[this.props.state.page].description}</p>
            <p>
              <img src={this.props.state.publications[this.props.state.page].image_url} alt="" className="img-fluid"/>
            </p>            
            <div dangerouslySetInnerHTML={{__html: x}} />
                
            <div className="about-author d-flex pt-5">
              <div className="bio align-self-md-center mr-4">
                <img src="https://scontent.fuio1-1.fna.fbcdn.net/v/t1.0-9/16708182_1445627885470903_7215861514568363341_n.jpg?_nc_cat=111&oh=9668985ca30f055c2860ecaf3b96834c&oe=5C587386" alt="Image placeholder" className="img-fluid mb-4"/>
              </div>
              <div className="desc align-self-md-center">
                <h3>Acerca del autor</h3>
                <p>Soy un estudiante universitario, con muchas ganas de aprender todo aquello realcionado con el mundo de la programación y la creación de websites, sistemas y aplicaciones móviles, me encanta hacer deporte en el gimnasio en mi tiempo libre y me gusta pasar tiempo con mi novia y mi familia.</p>
              </div>
            </div>

            <div className="pt-5 mt-5">
              <h3 className="mb-5">{this.props.state.comments.length} Comentarios</h3>
              <ul className="comment-list">                   
                   {comments}               
              </ul>                                
              <div className="comment-form-wrap pt-5">
                <h3 className="mb-5">Deja un comentario</h3>
                <form action="#" onSubmit={this.handleComment.bind(this)} className="bg-light p-4">

                  <div className="form-group">
                    <label>Comentario</label>
                    <input ref="commentComment" type="text" className="form-control"/>
                  </div>

                  <div className="form-group">
                    <input type="submit" value="Enviar" className="btn py-3 px-4 btn-primary"/>
                  </div>

                </form>
              </div>
            </div>
          </div>    
    );
        
    }
}

class Main extends React.Component {
constructor(props) {
    super(props);
    var self=this;    
    fetch("./site/publications")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({            
            publications: result
          });                     
        },
        (error) => {
          this.setState({            
            error
          });
        }
      );   
    $.ajax({
        url: "./site/user-logged",        
        success: function (response) {
            if(response!=0){
               self.setState({
                    logged:true,
                    username:response,                   
                 }); 
            }                                                          
        },
        error: function () {
            alert("algo salio mal");
        }
    });
    $.ajax({
        url: "./site/reload",        
        success: function (response) {
            if(response==false){
               window.location.reload();
            }                                                          
        },
        error: function () {
            alert("algo salio mal");
        }
    });
    this.state={      
      error: null,
      page:-1,      
      publications: [],
      comments:[],
      logged:false,
      username:null,
      publication:0
    };          
    this.submitComment=this.submitComment.bind(this);
    this.submitRegister=this.submitRegister.bind(this);
    this.submitLogin=this.submitLogin.bind(this);
    this.submitContact=this.submitContact.bind(this);
}

single(i,id){    
     fetch("./site/comments/"+id)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({            
            comments: result
          });                   
        },
        (error) => {
          this.setState({            
            error
          });
        }
      );   
    this.setState({
       page:i,
       publication:id
    });      
}

inicio(){
    this.setState({
       page:-1 
    });
}
about(){
    this.setState({
       page:-2 
    });
}
contact(){
    this.setState({
       page:-3
    });
}
login(){    
    this.setState({
       page:-4
    });    
}
register(){
    this.setState({
       page:-5 
    });
}
submitComment(comment){    
    if(this.state.logged==false){
        alert('Sólo usuarios loggeados pueden comentar');
    }else{
        var data = {username: this.state.username,publication:this.state.publication,comment:comment};
        var self=this;
        var page=this.state.page;
        this.setState({            
            page: -6
        });         
        $.ajax({
            url: "./site/put-comment",
            type: "POST",
            dataType: 'text',
            data: data,
            success: function () {                         
                fetch("./site/comments/"+self.state.publication)
                .then(res => res.json())
                .then(
                  (result) => {                                      
                    self.setState({            
                      comments: result,
                      page:page
                    });                   
                  },
                  (error) => {
                    self.setState({            
                      error
                    });
                  }
                );                 
            },
            error: function (e,textError,error) {
                alert("Error: "+e.status+" "+error);
                self.setState({                            
                    page:page
                  });
            }
        });      
    }
}
submitContact(e){
    e.preventDefault();
    var refs=this.refs;
    var data = {name: refs.contactName.value,email:refs.contactEmail.value,title:refs.contactTitle.value,message:refs.contactMessage.value};    
    var page=this.state.page;
    this.setState({            
        page: -6
    });
    var self=this;
    $.ajax({
        url: "./site/put-contact",
        type: "POST",
        dataType: 'text',
        data: data,
        success: function (response) {
            self.setState({                            
                page:page
              });                                                                 
            alert('Enviado correctamente');
        },
        error: function (e,textError,error) {
            alert("Error: "+e.status+" "+error);
            self.setState({                            
                page:page
              });
        }
    });     
}
submitRegister(e){
    e.preventDefault();
    var refs=this.refs;
    if(refs.registerPass.value==refs.registerRepass.value && refs.registerName.value!='' && refs.registerEmail.value!='' && refs.registerPass.value!='' && refs.registerRepass.value!='' && refs.registerPass.value.length>=8){
        var data = {name:refs.registerName.value,email:refs.registerEmail.value,pass:refs.registerPass.value,repass:refs.registerRepass.value};         
        var page=this.state.page;
        this.setState({            
            page: -6
        });
        var self=this;
        $.ajax({
            url: "./site/put-register",
            type: "POST",
            dataType: 'text',
            data: data,
            success: function (response) {                                                                  
                if(response==true){
                    alert("Usuario creado correctamente"); 
                    self.setState({
                       page:-4 
                    });
                }
            },
            error: function (e,textError,error) {
                alert("Error: "+e.status+" "+error);
                self.setState({                            
                    page:page
                  });
            }
        });          
    }else{
        alert("Por favor verifique que los campos se hayan llenado correctamente");
    }    
}
submitLogin(e){
    e.preventDefault();    
    var refs=this.refs;
    var self=this;
    if(refs.loginEmail.value!='' && refs.loginPass.value!='' && refs.loginPass.value.length>=8){
        var data = {email:refs.loginEmail.value,pass:refs.loginPass.value};
        var page=this.state.page;
        this.setState({            
            page: -6
        });
        $.ajax({
            url: "./site/login-front",
            type: "POST",
            dataType: 'text',
            data: data,
            success: function (response) {                                         
                if(response!=0){
                    alert("Ahora estás conectado");                    
                    self.setState({
                       logged:true,
                       page:-1,
                       username:response
                    });
                }else{
                    alert("Usuario o contraseña invalidos");
                }
            },
            error: function () {
                alert("algo salio mal");
                self.setState({                            
                    page:page
                  });
            }
        });
    }else{
        alert("Por favor verifique que los campos se hayan llenado correctamente");
    }    
}

logout(){
    var self=this;
    var page=this.state.page;
    this.setState({            
        page: -6
    });
    $.ajax({
        url: "./site/logout-front",        
        success: function (response) {                                         
            if(response==1){
                self.setState({
                    logged:false,
                    username:'',
                    page:page
                });              
            }else{
                alert("Algo no salió bien");
            }
        },
        error: function () {
            alert("algo salio mal");
            self.setState({                            
                page:page
              });
        }
    });           
}

render() {            
    
    const color={
        //background:'chocolate',
        color:'chocolate'
    }               
    const back={
        background:'chocolate'        
    }    
    const marginTop={
        marginTop:'50px'
    }
    const div1={
        textAlign:'center',
        height:'100%'
    }
    const div2={
        color: '#666666',
        marginTop:'50%',
        fontSize:'20px',
        fontWeight:'bold'
    }
    const div3={
        width:'30%'
    }
    const background1={
        backgroundImage: 'url("https://static.vivaolinux.com.br/imagens/fotos/logo.png")'
    }
    const background2={
        backgroundImage: 'url("https://images3.alphacoders.com/813/813087.jpg")'
    }
    let contenido;
    let comments;
    let indice;
    switch(this.state.page){
        case -1:
            contenido = this.state.publications.map((publication,i)=>{
                return(                                           
                           <div className="col-md-6" key={i}>
                             <div className="blog-entry">
                               <a href="#" onClick={this.single.bind(this,i,publication.id)} className="blog-image">
                                 <img src={publication.image_url} className="img-fluid"  />
                               </a>
                               <div className="text py-4">
                                 <div className="meta">
                                   <div><a href="#" style={color}>{publication.date}</a></div>
                                   <div><a href="#" style={color}>Oscar Vinueza</a></div>
                                 </div>
                                 <h3 className="heading"><a href="#" onClick={this.single.bind(this,i,publication.id)} style={color}>{publication.title}</a></h3>
                                 <p>{publication.description}</p>
                               </div>
                             </div>                    
                           </div>                                        
                 );
             });
            break;
        case -2:
            contenido= <div className="row about-section">
      			<div className="col-md-6">
      				<img src="https://d500.epimg.net/cincodias/imagenes/2017/07/07/midinero/1499441084_581226_1499441878_miniatura_normal.jpg" className="img-fluid" alt=""/>
      			</div>
      			<div className="col-md-6">
      				<h2 className="mb-4">Anubis</h2>
      				<p>Anubis es un blog estudiantil, en el cual se publica información interesante a cerca de temas tecnológicos en los que se ven envueltos estudiantes de 6to nivel de la carrera de Ingenierda en Sistemas y Computación de la Pontificia Universidad Católica del Ecuador con sede en Esmeraldas.</p>
      			</div>
      			<div className="col-md-12 mt-5">
      				<p>Nuestro objetivo es dar a conocer estos temas mostrando nuestras experiencias y describiendo como es el trabajo con cada una de las herramientas manejadas y los nuevos temas que a diario conocemos.</p>
      				<h3 className="mb-4 mt-5">Siguenos aquí</h3>
      				<ul className="ftco-footer-social list-unstyled">
                                    <li><a href="https://twitter.com/CodecWorld"><span className="icon-twitter"></span></a></li>
                                    <li><a href="https://www.facebook.com/Jostechq-211434169558564/"><span className="icon-facebook"></span></a></li>
                                    <li><a href="https://www.instagram.com/vinuezaceli/?hl=es-la"><span className="icon-instagram"></span></a></li>
                                </ul>
      			</div>
      		</div>
            break;
        case -3:
            contenido=<div>
                        <div className="col-md-12 mb-4">
                          <h1 style={color}>Información de contácto</h1>
                        </div>
                        <div className="w-100"></div>
                        <div className="col-md-6">
                          <p><span style={color}>Dirección:</span> Calle espejo, subida a santa cruz</p>
                        </div>
                        <div className="col-md-6">
                          <p><span style={color}>Teléfono:</span>0982657354</p>
                        </div>
                        <div className="col-md-12">
                          <p><span style={color}>Email:</span> os.vinueza94@gmail.com</p>
                        </div>                        
                        <div className="col-md-12 mb-5" style={marginTop}>
                        <form action="#" onSubmit={this.submitContact}>
                          <div className="form-group">
                          <span style={color}>Nombres:</span>
                            <input ref='contactName' type="text" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Correo:</span>
                            <input ref='contactEmail' type="email" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Título del mensaje:</span>
                            <input ref='contactTitle' type="text" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Mensaje:</span>
                            <textarea ref='contactMessage' name="" id="" cols="30" rows="7" className="form-control"></textarea>
                          </div>
                          <div className="form-group">
                            <input ref='contactName' type="submit" value="Enviar mensaje" className="btn btn-primary py-3 px-5"/>
                          </div>
                        </form>
                      </div>
                    </div>
            break;
        case -4:
            contenido=<div>
                        <div className="col-md-12 mb-4">
                          <h1 style={color}>Login</h1>
                        </div>
                        <div className="w-100"></div>
                        <div className="col-md-12">
                          <p>Por favor llena los siguientes campos con tus credenciales</p>
                        </div>                                                
                        <div className="col-md-12 mb-5" style={marginTop}>
                        <form action="#" onSubmit={this.submitLogin}>
                          <div className="form-group">
                          <span style={color}>Correo:</span>
                            <input ref='loginEmail' type="email" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Contraseña:</span>
                            <input ref='loginPass' type="password" className="form-control"/>
                          </div>                          
                          <div className="form-group">
                            <input type="submit" value="Ingresar" className="btn btn-primary py-3 px-5"/>
                          </div>
                        </form>
                      </div>
                    </div>
            break;
        case -5:
            contenido=<div>
                        <div className="col-md-12 mb-4">
                          <h1 style={color}>Registro</h1>
                        </div>
                        <div className="w-100"></div>
                        <div className="col-md-12">
                          <p>Por favor llena los siguientes campos con tus datos personales</p>
                        </div>                                                
                        <div className="col-md-12 mb-5" style={marginTop}>
                        <form action="#" onSubmit={this.submitRegister}>
                          <div className="form-group">
                          <span style={color}>Nombres:</span>
                            <input ref='registerName' type="text" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Correo:</span>
                            <input ref='registerEmail' type="email" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Contraseña:</span> (mín 8 caracteres)
                            <input ref='registerPass' type="password" className="form-control"/>
                          </div>
                          <div className="form-group">
                          <span style={color}>Repetir Contraseña:</span>
                            <input ref='registerRepass' type="password" className="form-control"/>
                          </div>                          
                          <div className="form-group">
                            <input type="submit" value="Registrarse" className="btn btn-primary py-3 px-5"/>
                          </div>
                        </form>
                      </div>
                    </div>
            break;
        case -6:
            contenido= <div id="WindowLoad"><div style={div1}><div  style={div2}>Cargando....</div><img style={div3}  src='images/load.gif'/></div></div>
            break;
        default:                
           contenido = <Publication state={this.state} sendComment={this.submitComment}/>
    } 
    
    if(this.state.logged==true){
        indice= <ul className="mb-5">                      
                      <li><a href="#" onClick={this.inicio.bind(this)}><span>Inicio</span></a></li>
                      <li><a href="#" onClick={this.about.bind(this)}><span>Acerca de nosotros</span></a></li>                      
                      <li><a href="#" onClick={this.contact.bind(this)}><span>Contácto</span></a></li>
                      <li><a href="#" onClick={this.logout.bind(this)}><span>Logout</span></a></li>
                    </ul>
    }else{
        indice= <ul className="mb-5">                      
                      <li><a href="#" onClick={this.inicio.bind(this)}><span>Inicio</span></a></li>
                      <li><a href="#" onClick={this.about.bind(this)}><span>Acerca de nosotros</span></a></li>                      
                      <li><a href="#" onClick={this.contact.bind(this)}><span>Contácto</span></a></li>                     
                      <li><a href="#" onClick={this.login.bind(this)}><span>Login</span></a></li>                                                                    
                      <li><a href="#" onClick={this.register.bind(this)}><span>Registro</span></a></li>                      
                    </ul>
    }
    
    return (
            <div className="App">
        <nav id="colorlib-main-nav" role="navigation" style={back}>
            <a href="#" className="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
            <div className="js-fullheight colorlib-table">
              <div className="colorlib-table-cell js-fullheight">
                <div className="row d-flex justify-content-end">
                  <div className="col-md-12 px-5">
                        {indice}
                  </div>                  
                </div>
              </div>
            </div>
          </nav> 
          <div id="colorlib-page">
            <header>
            <div className="container-fluid">
                    <div className="row no-gutters">
                      <div className="col-md-12">
                        <div className="colorlib-navbar-brand">
                          <a className="colorlib-logo" onClick={this.inicio.bind(this)} href="#">Anubis</a>
                        </div>
                        <a href="#" className="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
                      </div>
                    </div>
            </div>
          </header>
            <section className="ftco-fixed clearfix">
              <div className="image js-fullheight float-left">
                <div className="home-slider owl-carousel js-fullheight">
                  <div className="slider-item js-fullheight" style={background1}>
                    <div className="overlay" />
                    <div className="container">
                      <div className="row slider-text align-items-end" data-scrollax-parent="true">
                        <div className="col-md-10 col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                          <p className="cat"><span>Yii2</span></p>
                          <h1 className="mb-3" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Nuevo Blog</h1>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slider-item js-fullheight" style={background2}>
                    <div className="overlay" />
                    <div className="container">
                      <div className="row slider-text align-items-end" data-scrollax-parent="true">
                        <div className="col-md-10 col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                          <p className="cat"><span>Sql</span></p>
                          <h1 className="mb-3" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">React es bakan</h1>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="page-container float-right">
                <div className="row">                
                    {contenido}
                </div>                
              </div>
            </section>
          </div>                 
        </div> 
    );
}
}


let domContainer = document.querySelector('#root');
ReactDOM.render(<Main />, domContainer);
