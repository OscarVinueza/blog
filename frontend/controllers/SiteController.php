<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Publication;
use yii\data\Pagination;
/**
 * Site controller
 */
class SiteController extends Controller
{
    
    public $enableCsrfValidation = false;
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('react');
    }
    
    public function actionBlog($id)
    {                
        $model=new \common\models\Comment();
        $publicacion= Publication::findOne($id);
        if($model->load(Yii::$app->request->post())){
            $model->date= date('j/n/Y');
            $model->publication_id=$publicacion->id;
            $model->save();
            return $this->refresh();
        }else{
            $comentarios= \common\models\Comment::find()->where(['publication_id'=>$publicacion->id])->all();            
            return $this->render('blog-single',[
                'publicacion'=>$publicacion,
                'model'=>$model,
                'comentarios'=>$comentarios
            ]);          
        }        
    }
    
    
    public function actionReloj()
    {
        return $this->render('reloj');
    }        
    
    public function actionPublications()
    {        
        $publications= Publication::find()->where(['status'=>1])->orderBy(['id'=>SORT_DESC])->asArray()->all();             
        return json_encode($publications);
    }
    
    public function actionComments($id)
    {
        $comentarios= \common\models\Comment::find()->where(['publication_id'=>$id])->asArray()->all();         
        return json_encode($comentarios);
    }
    
    public function actionReload()
    {
        if(Yii::$app->session->isActive){
           return true; 
        }else{
            return false;
        }
    }
    
    public function actionPutComment()
    {        
        $request=Yii::$app->request;
        $comentario=new \common\models\Comment();
        $comentario->comment=$request->post('comment');
        $comentario->name=$request->post('username');
        $comentario->date=date('j/n/Y');
        $comentario->publication_id=$request->post('publication');
        $comentario->save();                 
        return 1;
    }
    
    public function actionPutRegister()
    {        
        $request=Yii::$app->request;
        $user= new \common\models\User();        
        $user->nombre=$request->post('name');               
        $user->username=$request->post('email');       
        $user->password_hash=Yii::$app->getSecurity()->generatePasswordHash($request->post('pass'));        
        $user->role=0;
        $user->description=null;
        $user->image=null;
        $user->auth_key=null;        
        $user->status=10;        
        $user->save();
        return true;
    }
    
    public function actionLoginFront()
    {                        
        $request=Yii::$app->request;               
        $email=$request->post('email');
        $password=$request->post('pass');        
        $user= \common\models\User::find()->where(['username'=>$email])->one();        
        if (Yii::$app->getSecurity()->validatePassword($password, $user->password_hash)) {
            Yii::$app->session['logged']=true;
            Yii::$app->session['user_id']=$user->id;            
            return $user->nombre;
        } else {
            return 0;
        }
    }
    
    public function actionUserLogged()
    {        
        if(Yii::$app->session['logged']==true){            
            return \common\models\User::findOne(Yii::$app->session['user_id'])->nombre;
        }else{
            return 0;
        }
    }
    
    public function actionLogoutFront()
    {        
        Yii::$app->session->destroy();
        return 1;
    }
    
    public function actionPutContact()
    {        
        $request=Yii::$app->request;
        $contact= new \common\models\Contact();
        $contact->name=$request->post('name');
        $contact->email=$request->post('email');
        $contact->subject=$request->post('title');
        $contact->message=$request->post('message');
        $contact->save();
        $message = Yii::$app->mailer->compose();                
        $message->setTo($contact->email);                
        $message->setFrom(Yii::$app->params['adminEmail']);                
        $message->setSubject('Respuesta de Anubis.com');
        $message->setHtmlBody('Gracias por contactarte con nosotros, responderemos pronto tu solicitud <img src="https://st3.depositphotos.com/5623324/16825/v/600/depositphotos_168252644-stock-video-egyptian-god-of-death-anubis.jpg" style="width:100%"/>');
        $message->send();
        return 1;
    }
    
    public function actionMelon()
    {
        return $this->render('melon');
    }
    
    public function actionPixi()
    {
        return $this->render('pixi');
    }
    
    public function actionPhaser()
    {
        return $this->render('phaser');
    }
    
    public function actionError()
    {
        return $this->render('404');
    }
    
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionBlogItem()
    {
        return $this->render('blog-item');
    } 
      
    public function actionPortfolio()
    {
        return $this->render('portfolio');
    }
    
    public function actionPricing()
    {
        return $this->render('pricing');
    }
    
    public function actionSendEmail()
    {
        return $this->render('sendemail');
    }
    
    public function actionServices()
    {
        return $this->render('services');
    }
    
    public function actionShortCodes()
    {
        return $this->render('shortcodes');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new \common\models\Contact();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { 
            $secret="6LeO93YUAAAAAGBr3-ZNVYtlstwkgVfhZN_vLcru";
            $response=Yii::$app->request->post('g-recaptcha-response');
            $userIp=Yii::$app->request->userIP;
            
            $model->provincia_id=Yii::$app->request->post('Contact')['provincia_id'];
            $model->canton_id=Yii::$app->request->post('Contact')['canton_id'];
            
            $ch = curl_init();
 
            // definimos la URL a la que hacemos la petición
            curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
            // indicamos el tipo de petición: POST
            curl_setopt($ch, CURLOPT_POST, TRUE);
            // definimos cada uno de los parámetros
            curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=".$secret."&response=".$response."&remoteip=".$userIp);

            // recibimos la respuesta y la guardamos en una variable
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $recaptcha = curl_exec ($ch);

            // cerramos la sesión cURL
            curl_close ($ch);
            
            $respuesta=json_decode($recaptcha)->success;
                                    
            if($respuesta==true){
                $model->save();   
                
                $message = Yii::$app->mailer->compose();                
                $message->setTo($model->email);                
                $message->setFrom(Yii::$app->params['adminEmail']);                
                $message->setSubject('Respuesta de Anubis.com');
                $message->setHtmlBody('Gracias por contactarte con nosotros, responderemos pronto tu solicitud <img src="https://st3.depositphotos.com/5623324/16825/v/600/depositphotos_168252644-stock-video-egyptian-god-of-death-anubis.jpg" style="width:100%"/>');
                $message->send();                
                return $this->refresh();
                
            }elseif($model->provincia_id == "Seleccione provincia" || $model->canton_id == "Seleccione cantón" || $model->canton_id=="Seleccionar..."){                                       
                Yii::$app->getSession()->setFlash('error', 'Error al llenar los campos requeridos');   
                return $this->refresh();            
            }else{                                          
                Yii::$app->getSession()->setFlash('error', 'Error con la verificación del captcha');   
                return $this->refresh();
            }
                        
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
           
    public function actionProvincias($id)
    {
        $cantones=\common\models\Canton::find()->where(['idProvincia'=>$id])->all();        
        
        if($cantones>0)
        {
            echo "<option>" . Yii::t('app', 'Seleccionar...') . "</option>";
            foreach($cantones as $canton){                
                echo "<option value='" . $canton['idCanton'] . "'>" . $canton['nombreCanton'] . "</option>";
            }
        }
        else
        {
            echo "<option>" . Yii::t('app', 'No Option') . "</option>";
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
