<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "canton".
 *
 * @property int $idCanton
 * @property int $idProvincia
 * @property string $nombreCanton
 * @property int $codigoCanton
 * @property int $idRegion
 * @property string $fecha_creacion
 *
 * @property Provincia $provincia
 */
class Canton extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canton';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProvincia', 'codigoCanton', 'idRegion'], 'integer'],
            [['fecha_creacion'], 'safe'],
            [['nombreCanton'], 'string', 'max' => 80],
            [['idProvincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincia::className(), 'targetAttribute' => ['idProvincia' => 'idProvincia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCanton' => 'Id Canton',
            'idProvincia' => 'Id Provincia',
            'nombreCanton' => 'Nombre Canton',
            'codigoCanton' => 'Codigo Canton',
            'idRegion' => 'Id Region',
            'fecha_creacion' => 'Fecha Creacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia()
    {
        return $this->hasOne(Provincia::className(), ['idProvincia' => 'idProvincia']);
    }
}
