<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "provincia".
 *
 * @property int $idProvincia
 * @property int $idRegion
 * @property string $nombreProvincia
 * @property string $fecha_creacion
 *
 * @property Canton[] $cantons
 */
class Provincia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idRegion'], 'integer'],
            [['fecha_creacion'], 'safe'],
            [['nombreProvincia'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProvincia' => 'Id Provincia',
            'idRegion' => 'Id Region',
            'nombreProvincia' => 'Nombre Provincia',
            'fecha_creacion' => 'Fecha Creacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCantons()
    {
        return $this->hasMany(Canton::className(), ['idProvincia' => 'idProvincia']);
    }
}
