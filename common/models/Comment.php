<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $comment
 * @property string $name
 * @property string $date
 * @property int $publication_id
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment', 'name', 'date', 'publication_id'], 'required'],
            [['publication_id'], 'integer'],
            [['comment'], 'string', 'max' => 300],
            [['name'], 'string', 'max' => 100],
            [['date'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => 'Comentario',
            'name' => 'Nombre',
            'date' => 'Fecha',
            'publication_id' => 'Publication ID',
        ];
    }
}
