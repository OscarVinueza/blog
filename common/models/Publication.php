<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "publication".
 *
 * @property int $id
 * @property string $title
 * @property string $date
 * @property string $description
 * @property string $content
 * @property int $status
 * @property string $image_url
 * @property int $user_id
 */
class Publication extends \yii\db\ActiveRecord
{
    
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publication';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'date', 'description', 'content', 'status', 'image_url', 'user_id'], 'required'],
            [['status', 'user_id'], 'integer'],
            [['title', 'date'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
            [['content'], 'string', 'max' => 10000],
            [['image_url'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Título',
            'date' => 'Fecha',
            'description' => 'Descripción',
            'content' => 'Contenido',
            'status' => 'Estado',
            'image_url' => 'Url de la imagen',
            'user_id' => 'User ID',
        ];
    }
}
