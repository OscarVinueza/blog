<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $message
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'message'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['email', 'subject'], 'string', 'max' => 100],
            [['message'], 'string', 'max' => 300],
            //[['provincia_id','canton_id'], 'int', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'email' => 'Correo',
            'subject' => 'Título del mensaje',
            'message' => 'Mensaje',
            'provincia_id' => 'Provincia',
            'canton_id' => 'Cantón',
        ];
    }
}
