<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'username' => 'root',
            'dsn' => 'mysql:host=142.93.201.13:3306;dbname=blog',            
            'password' => '12345678',
            //'dsn' => 'mysql:host=localhost;dbname=blog',            
            //'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',            
            'useFileTransport' => false,
            'transport'=>[
                'class'=>'Swift_SmtpTransport',
                'host'=>'smtp.gmail.com',
                'username'=>'os.vinueza94@gmail.com',
                'password'=>'kgypzownyrwcakat',
                'port'=>'587',
                'encryption'=>'tls'
            ]
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => array(
               '<controller:\w+>/<id:\d+>' => '<controller>/view',
               '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
               '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
         ],
    ],
];
