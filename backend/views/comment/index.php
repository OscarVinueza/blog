<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comentarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if(Yii::$app->user->identity->role==1){ echo Html::a('Crear Comentario', ['create'], ['class' => 'btn btn-success']);}?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'comment',
            'name',
            'date',
            //'publication_id',

            ['class' => 'yii\grid\ActionColumn','template'=>(Yii::$app->user->identity->role==1)? '{view} {update} {delete}' : '{view}'],
        ],
    ]); ?>
</div>
