<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PublicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Publicaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publication-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if(Yii::$app->user->identity->role==1){ echo Html::a('Crear publicación', ['create'], ['class' => 'btn btn-success']);}?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            'date',
            'description',
            //'content',
            //'status',
            //'image_url:url',
            //'user_id',

            ['class' => 'yii\grid\ActionColumn','template'=>(Yii::$app->user->identity->role==1)? '{view} {update} {delete}' : '{view}'],
        ],
    ]); ?>
</div>
